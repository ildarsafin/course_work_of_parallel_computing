﻿namespace Kurs_AI
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SortPTButton = new System.Windows.Forms.Button();
            this.SortSTButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.thresholdTextBox = new System.Windows.Forms.TextBox();
            this.elemCountTextBox = new System.Windows.Forms.TextBox();
            this.mergeGroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mThreadCountTextbox = new System.Windows.Forms.TextBox();
            this.MergeMTButton = new System.Windows.Forms.Button();
            this.MergeSTButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.mergeElCountTextBox = new System.Windows.Forms.TextBox();
            this.checkArrayButton = new System.Windows.Forms.Button();
            this.clearLogButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.mergeGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(235, 12);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.Size = new System.Drawing.Size(213, 340);
            this.logTextBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SortPTButton);
            this.groupBox1.Controls.Add(this.SortSTButton);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.thresholdTextBox);
            this.groupBox1.Controls.Add(this.elemCountTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 114);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "QuickSort";
            // 
            // SortPTButton
            // 
            this.SortPTButton.Location = new System.Drawing.Point(106, 79);
            this.SortPTButton.Name = "SortPTButton";
            this.SortPTButton.Size = new System.Drawing.Size(88, 23);
            this.SortPTButton.TabIndex = 5;
            this.SortPTButton.Text = "Sort parallel";
            this.SortPTButton.UseVisualStyleBackColor = true;
            this.SortPTButton.Click += new System.EventHandler(this.SortPTButton_Click);
            // 
            // SortSTButton
            // 
            this.SortSTButton.Location = new System.Drawing.Point(6, 79);
            this.SortSTButton.Name = "SortSTButton";
            this.SortSTButton.Size = new System.Drawing.Size(90, 23);
            this.SortSTButton.TabIndex = 4;
            this.SortSTButton.Text = "Sort single";
            this.SortSTButton.UseVisualStyleBackColor = true;
            this.SortSTButton.Click += new System.EventHandler(this.SortSTButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Parallel threshold:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Element count:";
            // 
            // thresholdTextBox
            // 
            this.thresholdTextBox.Location = new System.Drawing.Point(106, 49);
            this.thresholdTextBox.Name = "thresholdTextBox";
            this.thresholdTextBox.Size = new System.Drawing.Size(88, 20);
            this.thresholdTextBox.TabIndex = 1;
            this.thresholdTextBox.Text = "10000";
            // 
            // elemCountTextBox
            // 
            this.elemCountTextBox.Location = new System.Drawing.Point(107, 23);
            this.elemCountTextBox.Name = "elemCountTextBox";
            this.elemCountTextBox.Size = new System.Drawing.Size(88, 20);
            this.elemCountTextBox.TabIndex = 0;
            this.elemCountTextBox.Text = "1000000";
            this.elemCountTextBox.TextChanged += new System.EventHandler(this.elemCountTextBox_TextChanged);
            // 
            // mergeGroupBox
            // 
            this.mergeGroupBox.Controls.Add(this.label4);
            this.mergeGroupBox.Controls.Add(this.mThreadCountTextbox);
            this.mergeGroupBox.Controls.Add(this.MergeMTButton);
            this.mergeGroupBox.Controls.Add(this.MergeSTButton);
            this.mergeGroupBox.Controls.Add(this.label3);
            this.mergeGroupBox.Controls.Add(this.mergeElCountTextBox);
            this.mergeGroupBox.Location = new System.Drawing.Point(12, 133);
            this.mergeGroupBox.Name = "mergeGroupBox";
            this.mergeGroupBox.Size = new System.Drawing.Size(200, 114);
            this.mergeGroupBox.TabIndex = 2;
            this.mergeGroupBox.TabStop = false;
            this.mergeGroupBox.Text = "MergeSort";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Parallel threshold:";
            // 
            // mThreadCountTextbox
            // 
            this.mThreadCountTextbox.Location = new System.Drawing.Point(106, 49);
            this.mThreadCountTextbox.Name = "mThreadCountTextbox";
            this.mThreadCountTextbox.Size = new System.Drawing.Size(88, 20);
            this.mThreadCountTextbox.TabIndex = 8;
            this.mThreadCountTextbox.Text = "200000";
            // 
            // MergeMTButton
            // 
            this.MergeMTButton.Location = new System.Drawing.Point(106, 79);
            this.MergeMTButton.Name = "MergeMTButton";
            this.MergeMTButton.Size = new System.Drawing.Size(88, 23);
            this.MergeMTButton.TabIndex = 7;
            this.MergeMTButton.Text = "Sort parallel";
            this.MergeMTButton.UseVisualStyleBackColor = true;
            this.MergeMTButton.Click += new System.EventHandler(this.MergeMTButton_Click);
            // 
            // MergeSTButton
            // 
            this.MergeSTButton.Location = new System.Drawing.Point(6, 79);
            this.MergeSTButton.Name = "MergeSTButton";
            this.MergeSTButton.Size = new System.Drawing.Size(90, 23);
            this.MergeSTButton.TabIndex = 6;
            this.MergeSTButton.Text = "Sort single";
            this.MergeSTButton.UseVisualStyleBackColor = true;
            this.MergeSTButton.Click += new System.EventHandler(this.MergeSTButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Element count:";
            // 
            // mergeElCountTextBox
            // 
            this.mergeElCountTextBox.Location = new System.Drawing.Point(107, 22);
            this.mergeElCountTextBox.Name = "mergeElCountTextBox";
            this.mergeElCountTextBox.Size = new System.Drawing.Size(88, 20);
            this.mergeElCountTextBox.TabIndex = 3;
            this.mergeElCountTextBox.Text = "1000000";
            this.mergeElCountTextBox.TextChanged += new System.EventHandler(this.mergeElCountTextBox_TextChanged);
            // 
            // checkArrayButton
            // 
            this.checkArrayButton.Location = new System.Drawing.Point(18, 263);
            this.checkArrayButton.Name = "checkArrayButton";
            this.checkArrayButton.Size = new System.Drawing.Size(194, 38);
            this.checkArrayButton.TabIndex = 3;
            this.checkArrayButton.Text = "Check array sorting";
            this.checkArrayButton.UseVisualStyleBackColor = true;
            this.checkArrayButton.Click += new System.EventHandler(this.checkArrayButton_Click);
            // 
            // clearLogButton
            // 
            this.clearLogButton.Location = new System.Drawing.Point(18, 307);
            this.clearLogButton.Name = "clearLogButton";
            this.clearLogButton.Size = new System.Drawing.Size(194, 38);
            this.clearLogButton.TabIndex = 4;
            this.clearLogButton.Text = "Clear log";
            this.clearLogButton.UseVisualStyleBackColor = true;
            this.clearLogButton.Click += new System.EventHandler(this.clearLogButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 364);
            this.Controls.Add(this.clearLogButton);
            this.Controls.Add(this.checkArrayButton);
            this.Controls.Add(this.mergeGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.logTextBox);
            this.Name = "Form1";
            this.Text = "Parallel Quicksort testing";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mergeGroupBox.ResumeLayout(false);
            this.mergeGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SortPTButton;
        private System.Windows.Forms.Button SortSTButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox thresholdTextBox;
        private System.Windows.Forms.TextBox elemCountTextBox;
        private System.Windows.Forms.GroupBox mergeGroupBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox mergeElCountTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox mThreadCountTextbox;
        private System.Windows.Forms.Button MergeMTButton;
        private System.Windows.Forms.Button MergeSTButton;
        private System.Windows.Forms.Button checkArrayButton;
        private System.Windows.Forms.Button clearLogButton;
    }
}

