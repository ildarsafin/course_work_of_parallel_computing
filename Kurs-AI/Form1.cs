﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using System.Threading;

namespace Kurs_AI
{
    public partial class Form1 : Form
    {
        #region Variables
            int[] mas = new int[0];
            int threshold = 2048;
            int thresholdMerge = 8;
        #endregion

        #region General Functions
            public Form1()
            {  
                InitializeComponent();
            }

            private void InitializeVariables( Algorithms alg)
            {
                Random rand = new Random();
                int elemCount = 0;
                switch (alg)
                {
                    case Algorithms.QUICKSORT:
                        int thresh = 0;
                        try
                        {
                            elemCount = Convert.ToInt32(elemCountTextBox.Text);
                            thresh = Convert.ToInt32(thresholdTextBox.Text);
                        }
                        catch (Exception ex)
                        {
                            Log("Invalid Element Count or Threshold value.");
                            return;
                        }
                        threshold = thresh;
                        mas = new int[elemCount];

                        for (int i = 0; i < mas.Length; i++)
                        {
                            mas[i] = rand.Next(0, elemCount);
                        }
                        break;
                    case Algorithms.MERGESORT:
                        int _thresh = 0;
                        try
                        {
                            elemCount = Convert.ToInt32(mergeElCountTextBox.Text);
                            _thresh = Convert.ToInt32(mThreadCountTextbox.Text);
                        }
                        catch (Exception ex)
                        {
                            Log("Invalid Element Count or Threshold value.");
                            return;
                        }
                        thresholdMerge = _thresh;
                        mas = new int[elemCount];

                        rand = new Random();
                        for (int i = 0; i < mas.Length; i++)
                        {
                            mas[i] = rand.Next(0, elemCount);
                        }
                        break;
                }
            }
            private enum Algorithms
            {
                QUICKSORT,
                MERGESORT
            }

            private void Log(string message)
            {
                logTextBox.Text = "[" + DateTime.Now.TimeOfDay.Hours + ":" + DateTime.Now.TimeOfDay.Minutes + "]:  " + message + "\r\n" + logTextBox.Text;
                this.Refresh();
            }
        #endregion

        #region Quicksort functions
            private void QuickSortST(int[] a, int l, int r)
            {
                int temp;
                int x = a[l + (r - l) / 2];
                int i = l;
                int j = r;
                while (i <= j)
                {
                    while (a[i] < x) i++;
                    while (a[j] > x) j--;
                    if (i <= j)
                    {
                        temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                        i++;
                        j--;
                    }
                }
                if (i < r)
                    QuickSortST(a, i, r);

                if (l < j)
                    QuickSortST(a, l, j);
            }

            private void QuickSortMT(object paramList)
            {
                object[] parametres = (object[])paramList;
                int[] mass = (int[])parametres[0];
                int left = (int)parametres[1];
                int right = (int)parametres[2];
                CountdownEvent countdown = (CountdownEvent)parametres[3];

                int temp;
                int x = mass[left + (right - left) / 2];
                int i = left;
                int j = right;
                while (i <= j)
                {
                    while (mass[i] < x) i++;
                    while (mass[j] > x) j--;
                    if (i <= j)
                    {
                        temp = mass[i];
                        mass[i] = mass[j];
                        mass[j] = temp;
                        i++;
                        j--;
                    }
                }

                if (right - left < threshold)
                    QuickSortST(mass, left, right);
                else
                {
                    Thread[] threads = new Thread[2];

                    if (i < right)
                    {
                        countdown.AddCount();
                        ThreadPool.QueueUserWorkItem(objList =>
                        {
                            QuickSortMT(objList);
                        }, new object[] { (object)mas, (object)i, (object)right, (object)countdown });
                    }

                    if (left < j)
                    {
                        countdown.AddCount();
                        ThreadPool.QueueUserWorkItem(objList =>
                        {
                            QuickSortMT(objList);
                        }, new object[] { (object)mas, (object)left, (object)j, (object)countdown });
                    }
                }
                countdown.Signal();
            }
        #endregion

        #region Mergesort functions
            private void MergeSort(int[] input, int low, int high)
            {
                if (low < high)
                {
                    int middle = (low + high) / 2;
                    MergeSort(input, low, middle);
                    MergeSort(input, middle + 1, high);
                    Merge(input, low, middle, high);
                }
            }
            private void Merge(int[] input, int low, int middle, int high)
            {

                int left = low;
                int right = middle + 1;
                int[] tmp = new int[(high - low) + 1];
                int tmpIndex = 0;

                while ((left <= middle) && (right <= high))
                {
                    if (input[left] < input[right])
                    {
                        tmp[tmpIndex] = input[left];
                        left = left + 1;
                    }
                    else
                    {
                        tmp[tmpIndex] = input[right];
                        right = right + 1;
                    }
                    tmpIndex = tmpIndex + 1;
                }

                if (left <= middle)
                {
                    while (left <= middle)
                    {
                        tmp[tmpIndex] = input[left];
                        left = left + 1;
                        tmpIndex = tmpIndex + 1;
                    }
                }

                if (right <= high)
                {
                    while (right <= high)
                    {
                        tmp[tmpIndex] = input[right];
                        right = right + 1;
                        tmpIndex = tmpIndex + 1;
                    }
                }

                for (int i = 0; i < tmp.Length; i++)
                {
                    input[low + i] = tmp[i];
                }

            }            
        
            private void MergeSortMT(object paramList)
            {
                object[] parametres = (object[])paramList;
                int[] input = (int[])parametres[0];
                int low = (int)parametres[1];
                int high = (int)parametres[2];
                CountdownEvent countdown = (CountdownEvent)parametres[3];

                if (high - low < thresholdMerge)
                {
                    if (low < high)
                    {
                        int middle = (low + high) / 2;
                        MergeSort(input, low, middle);
                        MergeSort(input, middle + 1, high);
                        Merge(input, low, middle, high);
                    }
                }
                else
                {
                    Thread[] threads = new Thread[2];

                    if (low < high)
                    {
                        int middle = (low + high) / 2;

                         /*
                          * using (var countdownEvent = new CountdownEvent(2))
                        {
                            ThreadPool.QueueUserWorkItem(objList =>
                            {
                                MergeSortMT2(objList);
                            }, new object[] { (object)input, (object)(middle + 1), (object)high, (object)countdownEvent });
                            ThreadPool.QueueUserWorkItem(objList =>
                            {
                                MergeSortMT2(objList);
                            }, new object[] { (object)input, (object)low, (object)middle, (object)countdownEvent });
                            countdownEvent.Wait();
                        }*/                       
                        
                        countdown.AddCount();

                        threads[0] = new Thread(MergeSortMT);
                        threads[0].Start(new object[] 
                        { 
                            (object)input, 
                            (object)low, 
                            (object)middle, 
                            (object)countdown 
                        });

                        countdown.AddCount();

                        threads[1] = new Thread(MergeSortMT);
                        threads[1].Start(new object[] 
                        { 
                            (object)input, 
                            (object)(middle + 1), 
                            (object)high, 
                            (object)countdown 
                        });

                        threads[0].Join();
                        threads[1].Join();

                        Merge(input, low, middle, high);
                    }
                }
                countdown.Signal();
            }
            // ParametrizedThreadStart delegate version for MergeSort
            private void MergeSort(object inputObject)
            {
                object[] parametres = (object[])inputObject;
                int[] input = (int[])parametres[0];
                int low = (int)parametres[1];
                int high = (int)parametres[2];

                if (low < high)
                {
                    int middle = (low + high) / 2;
                    MergeSort(input, low, middle);
                    MergeSort(input, middle + 1, high);
                    Merge(input, low, middle, high);
                }
            }
        #endregion

        #region Event handlers
            private void SortSTButton_Click(object sender, EventArgs e)
            {
                // Creating new test array
                InitializeVariables(Algorithms.QUICKSORT);

                Stopwatch watch = new Stopwatch();
                watch.Start();
                QuickSortST(mas, 0, mas.Length - 1);
                watch.Stop();
                Log("QuickSort - ST: " + 
                    watch.Elapsed.TotalMilliseconds.ToString() + " ms");
            }
            private void SortPTButton_Click(object sender, EventArgs e)
            {
                // Creating new test array
                InitializeVariables(Algorithms.QUICKSORT);

                Stopwatch watch = new Stopwatch();
                watch.Start();

                using (var countdownEvent = new CountdownEvent(1))
                {
                    ThreadPool.QueueUserWorkItem(objList =>
                    {
                        QuickSortMT(objList);
                    }, new object[] { (object)mas, (object)0, (object)(mas.Length - 1), (object)countdownEvent });

                    countdownEvent.Wait();
                }

                watch.Stop();
                Log("QuickSort - MT: " +
                    watch.Elapsed.TotalMilliseconds.ToString() + " ms");
            }
        
            private void MergeSTButton_Click(object sender, EventArgs e)
            {
                // Creating new test array
                InitializeVariables(Algorithms.MERGESORT);

                Stopwatch watch = new Stopwatch();
                watch.Start();
                MergeSort(mas, 0, mas.Length - 1);
                watch.Stop();
                Log("MergeSort - ST: " +
                    watch.Elapsed.TotalMilliseconds.ToString() + " ms");
            }
            private void MergeMTButton_Click(object sender, EventArgs e)
            {
                // Creating new test array
                InitializeVariables(Algorithms.MERGESORT);
                int counter = 1, sum = 1, elements = mas.Length;
                while (elements > thresholdMerge)
                {
                    elements /= 2;
                    sum += (int)Math.Pow((double)2, (double)counter);   // вот в такие моменты ненавидишь c#
                    counter++;                                          // ну вот нельзя по-человечески 2^counter
                }

                // Displaying a confirmation dialog if a user is about to run MergeSort with too mant threads
                if (sum > 50)
                {
                    var confirmResult = MessageBox.Show("With current threshold settings MergeSort will use " + sum +
                        " threads. It will either take very long or it will crash. Are you sure?", "Warning", MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.No)
                    {
                        return;
                    }
                }

                Log("MergeSort will use " + sum + " threads");

                Stopwatch watch = new Stopwatch();
                watch.Start();

                using (var countdownEvent = new CountdownEvent(1))
                {
                    ThreadPool.QueueUserWorkItem(objList =>
                    {
                        MergeSortMT(objList);
                    }, new object[] { (object)mas, (object)0, (object)(mas.Length - 1), (object)countdownEvent });

                    countdownEvent.Wait();
                }

                watch.Stop();
                Log("MergeSort - MT: " +
                    watch.Elapsed.TotalMilliseconds.ToString() + " ms");
            }
        
            private void checkArrayButton_Click(object sender, EventArgs e)
            {
                if (mas.Length == 0)
                {
                    Log("Array wasn't initialized");
                    return;
                }
                for (int i = 1; i < mas.Length; i++)
                {
                    if (mas[i - 1] > mas[i])
                    {
                        Log("Array is sorted incorrectly");
                        return;
                    }
                }
                Log("Array is sorted correctly");
            }
            private void clearLogButton_Click(object sender, EventArgs e)
            {
                logTextBox.Text = "";
            }

            private void elemCountTextBox_TextChanged(object sender, EventArgs e)
            {
                try
                {
                    thresholdTextBox.Text = (Convert.ToInt32(elemCountTextBox.Text) / 100).ToString();
                }
                catch
                {
                    Log("Don't write anything weird in there!");
                }
            }
            private void mergeElCountTextBox_TextChanged(object sender, EventArgs e)
            {
                try
                {
                    mThreadCountTextbox.Text = (Convert.ToInt32(mergeElCountTextBox.Text) / 5).ToString();
                }
                catch
                {
                    Log("Don't write anything weird in there!");
                }
            }
        #endregion
    }
}
